/*
 * ListCheckFilter
 * Shows filter checkboxes to allow the user to filter checked 
 * and unhecked items or view all on the UI.
*/

import React from "react";

function ListCheckFilters( props ){
	return(
		<div className="todo-item-root list-group-item">
			<input 
				type="checkbox" 
				checked={ props.filterChecked } 
				onChange={ () => props.handlerCheckFilter( "checked" ) }
			/> Checked
			<input 
				type="checkbox" 
				checked={ props.filterUnchecked } 
				onChange={ () => props.handlerCheckFilter( "unchecked" ) } 
			/> unchecked
		</div>
	);
}

export default ListCheckFilters;