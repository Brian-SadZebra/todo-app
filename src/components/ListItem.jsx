	import React from "react";

/*
 * Converter a numberic priority value to a string. Priority has three levels
 * 1 = low, 2 = normal and 3 = high
*/
function convertNumPriority(priorityNum){
	if( priorityNum === 1 )
		return "Low";
	else if( priorityNum === 2 )
		return "Normal";
	else if( priorityNum === 3 )
		return "High";
	else
		return "Unknown Priority";
}

function ListItem( props ){
	const checkedStyle = {
		textDecoration: "line-through",
		color: "#909090"
	};

	const uncheckedStyle = {
		color: "black"
	};

	// console.log("props.item: ", props)
	
	return(
		<div className="todo-item-root list-group-item">
			<h4>Priority Level: { convertNumPriority( props.item.priority ) }</h4>
			<input className="todo-item-checkbox"
	            type="checkbox" 
	            checked={ props.item.checked } 
	            onChange={ () => props.handleChange( props.item.id, props.item.checked ) }
	        />
	        <div style={ props.item.checked ? checkedStyle : uncheckedStyle }>
				<p className="todo-item-title" >{ props.item.title }</p>
				<p>{ props.item.desc }</p>
			</div>
			<div className="item-function-btns">
				<button onClick={ () => props.handleDelete( props.item.id ) }>Delete</button>
				<button onClick={ () => props.handleEditBtnclick( props.item.id ) }>Edit</button>
			</div>
		</div>
	);
}

export default ListItem;