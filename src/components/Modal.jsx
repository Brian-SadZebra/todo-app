import React from "react";

function Modal( props ){
	let showHideClassName = props.show ? "modal-window display-block" : "modal-window display-none";
	return(
		<div className={showHideClassName}>
      		<section className="modal-main">
        		{props.children}
        		<br />
        		<button onClick={props.handleClose}>close</button>
      		</section>
    	</div>
	);
}

export default Modal;