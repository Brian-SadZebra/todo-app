import React from "react";
import ListItem from "./ListItem";
import ListCheckFilters from "./ListCheckFilters";
import ListPriorityFilters from "./ListPriorityFilters";
import AddItem from "./AddItem";
import Modal from "./Modal";

import todosData from "../dummyData/data";

class MyList extends React.Component {

	constructor(){
		super();
		// Set initial state, this is currently dummy data. Checked and unchecked filters are
		// set to true as default.
		this.state = {
			todos: this.initTodos( todosData ),
			filterChecked: true,
			filterUnchecked: true,
			priorityFilter: 0,
			show: false,
			modalType: "",
			currentEditId: null
		};
		// Bind handle change 
		this.handleChange = this.handleChange.bind(this); 
		this.handlerCheckFilter = this.handlerCheckFilter.bind(this); 
		this.handlePriorityFilter = this.handlePriorityFilter.bind(this);
		this.handleDelete = this.handleDelete.bind(this);
		this.handleAddItem = this.handleAddItem.bind(this);
		this.showModal = this.showModal.bind(this);
		this.hideModal = this.hideModal.bind(this);
		this.handleEditBtnclick = this.handleEditBtnclick.bind(this);
	}

	/*
	 * Method used to only set the state
	*/
	updateState( newState ){
		this.setState( newState );
	}

	// maps the initial data to add the showItem value, value is set to true
	// on all items by default
	initTodos( list ){
		// update the todolist state with a value to show the item.
		return list.map( todo => ({
			...todo, showItem: true
		}) );
	}

	/*
	 * Handles the change of the checkboxes for the list before updating the state
	 *
	 * NOTE: This function should be renamed, it's doesn't really explain what it does
	*/	
	handleChange(id) {
		const newState = this.state.todos.map(todo => {
            if (todo.id === id) {
                todo.checked = !todo.checked;
            }
            return todo;
        });
		this.updateState( newState ); 
    }

    // Orders the list of todos and passes it on to update the state
    getOrderedTodosFromState(){
    	let todosFromState = this.state.todos
    	// Order list
		const orderedList = todosFromState.sort((a, b) => (a.checked && !b.checked) ? 1 : -1);
		return orderedList;
    }

    // This function will check or uncheck the selected checkbox
    changedFilterChecked( checkbox ){
		if( checkbox === "checked" ){
    		this.updateState( { filterChecked: !this.state.filterChecked } )
    	}
    	else if( checkbox === "unchecked" ){
    		this.updateState( { filterUnchecked: !this.state.filterUnchecked } )
    	}
    }

    /*
	 * To handle when the user select to filter checked or unchecked
    */
    handlerCheckFilter( filterType ){
    	const newState = this.state.todos.map( ( todo ) => {
    		if( filterType === "checked" ){
    			this.changedFilterChecked("checked")
    		}
    		else if( filterType === "unchecked" ) {
    			this.changedFilterChecked("unchecked")
    		}
    		return todo;
    	} );
    	this.updateState( newState )
    }
  
    // handles filtering of the priority 
    handlePriorityFilter( priorityLevel ){
		let newPriorityLevel = priorityLevel;
		this.setState({ priorityFilter: newPriorityLevel });
    }

    getFilterTotals( filter ){
		let filtered = this.state.todos.filter( item => item.priority === filter );
    	return filtered.length;
    }

    handleDelete( id ){
    	let theState = this.state.todos.filter( item => item.id === id )

    	theState.map( item => item.deleted = true )

    	this.setState( theState )
    }

    getTotalItemsVisible(){
    	let theState = this.state.todos.filter( item => {
    		if( item.deleted !== true ){
    			return item;
    		}
    		return false;
    	} );
    	return theState.length;
    }

    handleAddItem( e ){
    	e.preventDefault();

    	 // This is a little hack for demo data, this will change in production
    	let newId 		= this.state.todos.length + 1;
    	let newState 	= this.state.todos;
    	let newItem 	= {
				    		"id": newId,
							"title": e.title.value,
							"desc": e.target.desc.value,
							"checked": false,
							"priority": parseInt( e.target.priority.value )
				    	};

    	newState[newState.length] = newItem;

    	// Add the new item to the state
    	this.setState( newState );
    }

    showModal = () => {
    	console.log("show modal")
    	this.setState({ show: true, modalType: "add" });
	};

  	hideModal = () => {
    	this.setState({ show: false });
  	};

  	// handles when the user clicking the button to edit and item
  	handleEditBtnclick = ( id ) => {
  		console.log("handleEditBtnclick ID: ", id)
  		this.setState({ show: true, modalType: "edit", currentEditId: id });
  	}

  	// Handles when the user has updated item data
  	handleUpdateItem = () => {
  		console.log("Handle update item");
  	}

  	// returns the header for the modal window based on the modalType state value
  	getModalHeader = ( type ) => {
  		if( type === "add" )
  			return "Add new item";
  		else if ( type === "edit" ){

  			return "Edit Item";
  		}

  		return;
  	}

  	getItemField = ( field ) => {
		const data = this.state.todos.filter( item => this.state.currentEditId === item.id )
  		if( field === "title" ){
  			// This is assuming that the ID is unique
  			return data[0].title;
  		}
  		else if( field === "desc" || field === "description" )
  			return data[0].desc;
  	}

  	getModalBtn = () => {
  		
  		if( this.state.modalType === "add" )
  			return <button>Add</button>
  		else if( this.state.modalType === "edit" )
  			return <button>Edit</button>

  		return
  	}

  	// Handles the editing of a list item
  	handleEdit = ( id, updatedItem ) => {
  		let todos = this.state.todos;
  		todos.map( item => {
  			if( item.id === id ){
  				// Update values
  				item.title = updatedItem.title;
  				item.desc = updatedItem.desc;
  				item.priority = updatedItem.priority
  			}
  			return item
  		} );
  		// Update the state with the edited item
  		this.setState({ todos: todos })
  	}

  	// Because we're reusing the same modal window we'll need to handle different
  	// action as we needto figure out what the form is doing before processing the
  	// relevant data and updating the state
  	handleSubmit = ( e, id ) => {
  		console.log("Handle Submit", this.state.currentEditId);
  		e.preventDefault()

  		console.log("Target: ", e.target.priority)

  		if( this.state.modalType === "add" ){
  			this.handleAddItem(e);
  		}
  		else if( this.state.modalType === "edit" ){
  			let updatedItem	= {
								"title": e.target.title.value,
								"desc": e.target.desc.value,
								"priority": parseInt( e.target.priority.value )
					    	};
  			this.handleEdit( id, updatedItem )
  		}
  	}

	render() {
		// Take a copy of the todos list from state and order it
		let 	todoList 			= this.getOrderedTodosFromState();
		const 	checkedFilter 		= this.state.filterChecked, 
				uncheckedFilter 	= this.state.filterUnchecked,
				totalItems 			= this.getTotalItemsVisible(),
				priorityTotals 		= {
					"low": this.getFilterTotals(1),
					"normal": this.getFilterTotals(2),
					"high": this.getFilterTotals(3),
					"all": totalItems
				};
		// Filter the list of todos by selected filters
		todoList = todoList.filter( item => {
			// Set the deleted value if it isn't present			
			let itemDeleted = item.deleted || false;

			if( item.checked === true && checkedFilter === true && 
				( this.state.priorityFilter === item.priority || this.state.priorityFilter === 0 ) &&
				(itemDeleted !== true) ){
				return item;
			}
			else if( item.checked === false && uncheckedFilter === true  && 
				( this.state.priorityFilter === item.priority || this.state.priorityFilter === 0 ) && 
				itemDeleted !== true ){
				return item;
			}
			return ( checkedFilter === true && uncheckedFilter === true && 
				( this.state.priorityFilter === item.priority || this.state.priorityFilter === 0 ) && 
				itemDeleted !== true ) ? item : false;
		} )

		const todoItems = todoList.map( item => {
				return (
					<ListItem 
						key={ item.id }
						item={ item }
						priorityLevel={ this.priority }
						handleChange={ this.handleChange }
						handleDelete={ this.handleDelete }
						handleEditBtnclick={ this.handleEditBtnclick }
					/>
				);
		})

		return (
			<div>
				<div className="list-group">
					<ListPriorityFilters handlePriorityFilter={ this.handlePriorityFilter } priorityTotals={ priorityTotals } />
					<div>viewing { todoItems.length } of { totalItems } items</div>
					<AddItem showModal={ this.showModal } />
					<ListCheckFilters 
						filterChecked={ this.state.filterChecked }
						filterUnchecked={ this.state.filterUnchecked }
						handlerCheckFilter={ this.handlerCheckFilter }
					/>
					{ todoItems }
				</div>


				{/*
				  * The code withing the <Modal></Modal> should be moved into it's own component.
				  * Handing a component that for each type of modal would be helpful.
				  *
				  * TODO: Make two new components, AddForm and editForm and add the correct component 
				  * 		within then tags here.
				*/}
				<Modal show={this.state.show} handleClose={this.hideModal}>
					<h1>{ this.getModalHeader( this.state.modalType ) }</h1>
					<form onSubmit={ (e) => this.handleSubmit(e, this.state.currentEditId) }>
						<div className="form-group">
							<label htmlFor="title">Title:</label>
							<input 
								type="input" 
								id="title" 
								defaultValue={ this.state.modalType === "edit" ? this.getItemField( "title" ) : "" }
							/>
						</div>
						<div className="form-group">
							<label htmlFor="desc">Description:</label>
							<input 
								type="input" 
								id="desc" 
								defaultValue={ this.state.modalType === "edit" ? this.getItemField( "desc" ) : "" } 
							/>
						</div>
						<div className="form-group">
							<label htmlFor="priority">priority:</label>
							<select id="priority">
								<option value="1">Low</option>
								<option value="2">Normal</option>
								<option value="3">High</option>
							</select>
						</div>
						{ this.getModalBtn() }
					</form>
				</Modal>
			</div>
		)
	}
}

export default MyList;