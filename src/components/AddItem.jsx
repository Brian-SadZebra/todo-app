import React from "react";

function addItem( props ){
	return(
		<div className="todo-item-root list-group-item">
			<button onClick={ () => props.showModal() }>Add Item</button>
		</div>
	);
}

export default addItem;