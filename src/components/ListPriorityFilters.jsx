/*
 * ListPriorityFilter
*/

import React from "react";

function ListPriorityFilters( props ){
	return(
		<div className="todo-item-root list-group-item">
			<label>Priority: </label>
			<select onChange={ ( event ) => props.handlePriorityFilter( parseInt( event.target.value ) ) }>
				<option value="0">All ({ props.priorityTotals.all })</option>
				<option value="1">Low ({ props.priorityTotals.low })</option>
				<option value="2">Normal ({ props.priorityTotals.normal })</option>
				<option value="3">high ({ props.priorityTotals.high })</option>
			</select>
		</div>
	);
}

export default ListPriorityFilters;