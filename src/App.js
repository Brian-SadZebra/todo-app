import React from 'react';
// import logo from './logo.svg';
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';

import MyList from "./components/MyList";
import ListHeader from "./components/ListHeader";
import ListFooter from "./components/ListFooter";
// import ListItem from "./components/ListItem"; 

function App() {
  return (
    <div>
      <ListHeader />
      <MyList />
      <ListFooter />
    </div>
  );
}

export default App;
