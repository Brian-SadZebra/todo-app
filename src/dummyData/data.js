const todosData = [
    {
      "id": 1,
      "title": "Item #1",
      "desc": "desc #1",
      "checked": false,
      "priority": 1,
      "deleted": true
    },
    {
      "id": 2,
      "title": "Item #2",
      "desc": "desc #2",
      "checked": true,
      "priority": 1
    },
    {
      "id": 3,
      "title": "Item #3",
      "desc": "desc #3",
      "checked": true,
      "priority": 2
    },
    {
      "id": 4,
      "title": "Item #2",
      "desc": "desc #2",
      "checked": false,
      "priority": 2
    },
    {
      "id": 5,
      "title": "Item #2",
      "desc": "desc #2",
      "checked": false,
      "priority": 3
    },
    {
      "id": 6,
      "title": "Item #2",
      "desc": "desc #2",
      "checked": false,
      "priority": 3
    },
];

export default todosData;